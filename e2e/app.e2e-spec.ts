import { DeviceCloudPage } from './app.po';

describe('device-cloud App', () => {
  let page: DeviceCloudPage;

  beforeEach(() => {
    page = new DeviceCloudPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
