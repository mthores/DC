import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { RouteModule } from 'app/router/router.module';
import { AppComponent } from 'app/app.component';
import { HomeComponent } from 'app/home/home.component';
import { NavbarComponent } from 'app/shared/navbar/navbar.component';
import { TodoComponent } from 'app/todo/todo.component';
import { TodoService } from 'app/services/todo.service';
import { DevicesComponent } from 'app/devices/devices.component';
import { DeviceService } from 'app/services/device.service';
import { DeviceListPipe } from 'app/shared/filters/devices/device-list.pipe';
import { DeviceEditComponent } from 'app/devices/device-edit/device-edit.component';
import { StatusPipePipe } from 'app/shared/filters/devices/status-pipe.pipe';
import { TicketsComponent } from 'app/tickets/tickets.component';
import { TicketService } from 'app/services/ticket.service';
import { TicketFormComponent } from 'app/tickets/ticket-form/ticket-form.component';
import { TicketsPipe } from 'app/shared/filters/tickets.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TodoComponent,
    NavbarComponent,
    TicketsComponent,
    TicketFormComponent,
    TicketsPipe,
    DevicesComponent,
    DeviceListPipe,
    DeviceEditComponent,
    DeviceListPipe,
    StatusPipePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouteModule
  ],
  providers: [TodoService, DeviceService, TicketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
