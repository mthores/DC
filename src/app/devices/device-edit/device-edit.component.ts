import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Device } from 'app/shared/models/device.model';
import { DeviceService } from 'app/services/device.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-device-edit',
  templateUrl: './device-edit.component.html',
  styleUrls: ['./device-edit.component.scss']
})
export class DeviceEditComponent implements OnInit {
  // Reactive form
  public deviceForm: FormGroup;

  // Inputs from parent component
  @Input() device: Device;
  @Input() edit: boolean;

  // Outputs to parent conponent
  @Output() close = new EventEmitter<boolean>();
  @Output() open = new EventEmitter<boolean>();

  // Constructor
  constructor(private deviceService: DeviceService) { } // Dependency Injection -> DeviceService

  ngOnInit() {
    // Instantiating formGroup
    this.deviceForm = new FormGroup({
      'id': new FormControl(null, Validators.required),
      'user': new FormControl(null, Validators.required),
      'type': new FormControl(null, Validators.required),
      'status': new FormControl(null, Validators.required)
    });
  }

  public toggleEdit() {
    this.edit = !this.edit;
  }

  // Submitting the recieved data from the Form
  submitDeviceForm(): void {
    if (this.deviceForm.valid) {
      const newDevice = this.deviceForm.value;
      // On update Device
      if (this.edit) {
        this.deviceService.updateDevice(newDevice, this.device);
        // On creating a new Device
      } else {
        this.deviceService.addDevice(newDevice);
      }
      this.edit = false;
    } else {
      this.deviceForm.get('id').markAsTouched();
      this.deviceForm.get('user').markAsTouched();
      this.deviceForm.get('type').markAsTouched();
      this.deviceForm.get('status').markAsTouched();
      console.log('form not valid');
    }
  }

  // Deleting Device
  deleteDevice() {
    this.deviceService.deleteDevice(this.device);
    this.edit = false;
    this.deviceForm.reset();
  }

  //
  defectDevice() {
    console.log('Fejlmeld knappen trykket!');
  }

  // Closing the detail container
  onClose() {
    this.close.emit(true);
    this.open.emit(true);
  }

}
