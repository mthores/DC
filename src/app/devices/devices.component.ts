import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, Input, OnDestroy, Output } from '@angular/core';
import { Device } from 'app/shared/models/device.model';
import { DeviceService } from 'app/services/device.service';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss']
})
export class DevicesComponent implements OnInit, OnDestroy {
  // Properties
  deviceList: Device[] = [];   // Array of devices
  selectedDevice: Device;      // Outputting to the device-edit.component
  edit = false;                // Outputting to the device-edit.component
  input = '';                  // Used for the filter / Search Box
  showAddButton = true;        // Toggle addButton on/off

  subscription: Subscription;

  // Constructor
  constructor(private deviceService: DeviceService) { } // Dependency Injection -> DeviceService

  ngOnInit() {
    this.deviceList = this.deviceService.getDevices(); // Recieving list of devices from DeviceService
    this.subscription = this.deviceService.devicesUpdated // Listening to changes, when devices are updated
      .subscribe(
      (devices: Device[]) => {
        this.deviceList = devices;
      }
      );
  }

  onCreateDevice() {
    const newDevice: Device = this.deviceService.createNewDevice();
    this.selectedDevice = newDevice;
    this.edit = true;
    // this.showAddButton = false;
  }

  onOpen(open: boolean) {
    this.showAddButton = open;
  }

  onEditDevice(device: Device) {
    this.edit = false;
    this.selectedDevice = device;
  }

  onClose(close: boolean) {
    if (close) {
      this.selectedDevice = null;
      this.edit = false;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
