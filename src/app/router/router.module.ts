import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "app/home/home.component";
import { DevicesComponent } from "app/devices/devices.component";
import { TicketsComponent } from "app/tickets/tickets.component";
import { TodoComponent } from "app/todo/todo.component";


const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'enheder', component: DevicesComponent
  },
  {
    path: 'sager', component: TicketsComponent
  },
  {
    path: 'opgaver', component: TodoComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class RouteModule { }
