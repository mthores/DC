import { EventEmitter } from '@angular/core';
import { Device } from 'app/shared/models/device.model';
import { Subject } from 'rxjs/Subject';

export class DeviceService {

  devicesUpdated = new Subject<Device[]>();
  // devicesSelected = new Subject<Device>();
  startedEditing = new Subject<Device>();


  private devices: any[] = [
    {
      id: '29384756',
      type: 'iPad',
      status: 'I brug',
      user: 'Bogi',
    },
    {
      id: '38475647',
      type: 'Chromebook',
      status: 'Til reparation',
      user: 'Mikkel'
    },
    {
      id: '38273654',
      type: 'Bærbar',
      status: 'I brug',
      user: 'Mark'
    },
    {
      id: '18273485',
      type: 'iPad',
      status: 'I brug',
      user: 'Bárður'
    },
    {
      id: '19283746',
      type: 'iPad',
      status: 'På lager',
      user: 'Ingen bruger'
    },
    {
      id: '29304958',
      type: 'iPad',
      status: 'Til reparation',
      user: 'Ingen bruger'
    },
    {
      id: '73820192',
      type: 'iPad',
      status: 'På lager',
      user: 'Ingen bruger'
    }
  ];

  getDevices() {
    return this.devices.slice();
  }

  getDevice(device: Device) {
    const index = this.devices.indexOf(device);
    return this.devices[index];
  }

  createNewDevice() {
    const newDevice = new Device (null, null, null, null);
    return newDevice;
    }

  addDevice(device: Device) {
    this.devices.push(device);
    this.devicesUpdated.next(this.devices.slice());
  }

  deleteDevice(device: Device) {
    const index = this.devices.indexOf(device);
    this.devices.splice(index, 1);
    this.devicesUpdated.next(this.devices.slice());
  }

  updateDevice(newDevice: Device, updatedDevice: Device) {
    const index = this.devices.indexOf(updatedDevice);
    this.devices.splice(index, 1, newDevice);
    this.devicesUpdated.next(this.devices.slice());
  }

}
