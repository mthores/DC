import { Injectable } from '@angular/core';
import { Ticket } from 'app/shared/models/ticket.model';
import { Observable, BehaviorSubject } from 'rxjs';
@Injectable()
export class TicketService {
  public tickets: Ticket[];
  private ticketCounter = 8;

  constructor() {
    this.tickets = this.generate();
  }

  addNewTicket(ticket: Ticket) {
    this.resultCheck(ticket);
    ticket.ticket_id = this.ticketCounter;
    ticket._id = Math.random();
    this.ticketCounter++;
    this.tickets.push(ticket);

  };

  editExsistingTicket(ticket: Ticket) {
    this.resultCheck(ticket);
    for (let counter = 0; counter < this.tickets.length; counter++) {
      if (this.tickets[counter].ticket_id === ticket.ticket_id) {
        this.tickets[counter] = ticket;
        break;
      }
    }
  }

  resultCheck(ticket: Ticket) {
    if (ticket.status.toString() === 'Resolved') {
      ticket.endDate = new Date();
    }
  }

  generate(): any[] {
    let test = [
      {
        createdBy_id: 'Mikkel',
        _id: 123123,
        ticket_id: 1,
        service_id: 1,
        title: 'Title',
        description: 'Test beskrivelse',
        startDate: "1494500931633",
        endDate: null,
        status: 'Open',
        devices: ['JHU887689']
      },
      {
        createdBy_id: 'Mark',
        _id: 123124,
        ticket_id: 2,
        service_id: 2,
        title: 'En Anden title',
        description: 'Test beskrivelse',
        startDate: "1494500931633",
        endDate: "1494676177809",
        status: 'Open',
        devices: ['JHU887639']
      },
      {
        createdBy_id: 'Bardur',
        _id: 123125,
        ticket_id: 3,
        service_id: null,
        title: 'En Anden title',
        description: 'Test beskrivelse',
        startDate: "1494500931633",
        endDate: "1494676177809",
        status: 'Open',
        devices: ['FAU887689']
      },
      {
        createdBy_id: 'Bogi',
        _id: 123126,
        ticket_id: 4,
        service_id: 2,
        title: 'En Anden title',
        description: 'Test beskrivelse',
        startDate: "1494500931633",
        endDate: "1494676177809",
        status: 'Open',
        devices: ['JKKI87689']
      },
      {
        createdBy_id: 'Mark',
        _id: 123127,
        ticket_id: 5,
        service_id: null,
        title: 'En Anden title',
        description: 'Test beskrivelse',
        startDate: "1494500931633",
        endDate: "1494676177809",
        status: 'Open',
        devices: ['GFD887J7H']
      },
      {
        createdBy_id: 'Mikkel',
        _id: 123128,
        ticket_id: 6,
        service_id: null,
        title: 'En Anden title',
        description: 'Test beskrivelse',
        startDate: "1494500931633",
        endDate: "1494676177809",
        status: 'Open',
        devices: ['YUC95S689']
      },
      {
        createdBy_id: 'Bogi',
        _id: 123129,
        ticket_id: 7,
        service_id: null,
        title: 'En Anden title',
        description: 'Test beskrivelse',
        startDate: "1494500931633",
        endDate: "1494676177809",
        status: 'Open',
        devices: ['J6GF7689']
      }
    ];
    return test;

  }
}
