import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Todo } from './../shared/models/todo.model';
import 'rxjs/Rx'; // for the map()

@Injectable()

export class TodoService {
  // URL to web API
  private iServiceUrl = 'http://angular2api2.azurewebsites.net/api/internships/';

  constructor(private http: Http) {}

  // Create Todo
  postTodo(item, done): Observable < any > {
    const data = new URLSearchParams();
    data.append('item', item);
    data.append('done', done);

    return this.http.post(this.iServiceUrl, data)
      .map(
        (response: Response) => {
          console.log(response);
          return response;
        }
      );
  } // End of Create

  // Update Todo
  putTodo(todo: Todo): Observable < any > {
    const data = new URLSearchParams();
    data.append('item', todo.item);
    data.append('done', "" + (!todo.done));
    data.append('_id', todo._id);
    const url = this.iServiceUrl + todo._id;
    return this.http.put(url, data)
      .map(
        (response: Response) => {
          console.log(response);
          return response;
        }
      );
  }

  getTodos(): Observable < any[] > {
    return this.http.get(this.iServiceUrl)
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }


  // Delete Todos
  deleteTodo(id: number): Observable < any > {
    const url = this.iServiceUrl + id;
    return this.http.delete(url)
      .map(
        (response: Response) => {
          return response;
        });
  }

}
