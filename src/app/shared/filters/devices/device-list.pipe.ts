import { Pipe, PipeTransform } from '@angular/core';
import { Device } from '../../models/device.model';

@Pipe({
  name: 'devicePipe'
})
export class DeviceListPipe implements PipeTransform {

  transform(items: Device[], filter: any): any {

    if (!items || !filter) {
      return items;
    }

    return items.filter(item => {
      // Filters through users
      if (item.user.toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
        return item;
      }
      // Filters through ID
      if (item.id.toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
        return item;
      }
      // Filters through brand name
      if (item.type.toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
        return item;
      }
    });
  }

}
