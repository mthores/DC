import { Pipe, PipeTransform } from '@angular/core';
import { Device } from "app/shared/models/device.model";

@Pipe({
  name: 'statusPipe'
})
export class StatusPipePipe implements PipeTransform {

  transform(items: Device[], storage: any, inUse: any, repair: any): any {

    if (!items || !storage && !inUse && !repair) {
      return items;
    }

    return items.filter(item => {
      if (storage && !inUse && !repair) {
        if (item.status === 'På lager') {
          return item;
        }
      }
      if (!storage && inUse && !repair) {
        if (item.status === 'I brug') {
          return item;
        }
      }
      if (!storage && !inUse && repair) {
        if (item.status === 'Til reparation') {
          return item;
        }
      }
      if (storage && inUse && !repair) {
        if (item.status === 'På lager' || item.status === 'I brug') {
          return item;
        }
      }
      if (storage && !inUse && repair) {
        if (item.status === 'På lager' || item.status === 'Til reparation') {
          return item;
        }
      }
      if (!storage && inUse && repair) {
        if (item.status === 'I brug' || item.status === 'Til reparation') {
          return item;
        }
      } else if (storage && inUse && repair) {
        if (item.status === 'I brug' || item.status === 'Til reparation' || item.status === 'På lager') {
          return item;
        }
      }
    });
  }
}

