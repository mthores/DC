import { Pipe, PipeTransform } from '@angular/core';
import { Ticket } from 'app/shared/models/ticket.model';

@Pipe({
  name: 'tickets'
})
export class TicketsPipe implements PipeTransform {

  transform(items: Ticket[], filter: any): any {
    if (!items || !filter) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter(item => {
      if (item.title.toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
        return item;
      }
      if (item.createdBy_id.toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
        return item;
      }
      if (('' + item.ticket_id).indexOf(filter.toLowerCase()) !== -1) {
        return item;
      }
    });
  }
}
