export class Device {

  constructor(
    public id: string,
    public type: string,
    public status: string,
    public user: string,
    public date_created?: Date
  ) {
    this.date_created = new Date(Date.now());
  }


}
