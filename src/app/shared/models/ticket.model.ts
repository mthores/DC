import { Device } from 'app/shared/models/device.model';

const enum Statuses { 'Open', 'Pending', 'Resolved' };

export class Ticket {
  nextTicketID = 10;

  static createNewTicket(): Ticket {
    const newTicket = new Ticket(
      null,
      null,
      null,
      null,
      '',
      '',
      new Date(Date.now()),
      'Open',
      []
    );
    return newTicket;
  }

  static createFromJSONArray(jArray: any[]): Ticket[] {
    const ticketArray: Ticket[] = [];
    for (let i = 0; i < jArray.length; i++) {
      let newTicket: Ticket;
      if (jArray[i].endDate === 'null') {
        newTicket = new Ticket(
          jArray[i].createdBy_id,
          jArray[i]._id,
          jArray[i].ticket_id,
          jArray[i].service_id,
          jArray[i].title,
          jArray[i].description,
          new Date(jArray[i].startDate),
          jArray[i].status,
          jArray[i].devices
        );
      } else {
        newTicket = new Ticket(
          jArray[i].createdBy_id,
          jArray[i]._id,
          jArray[i].ticket_id,
          jArray[i].service_id,
          jArray[i].title,
          jArray[i].description,
          new Date(jArray[i].startDate),
          jArray[i].status,
          jArray[i].devices,
          jArray[i].endDate
        );
      }
      ticketArray.push(newTicket);
    }
    return ticketArray;
  }

  constructor(
    public createdBy_id: string,
    public _id: number,
    public ticket_id: number,
    public service_id: number,
    public title: string,
    public description: string,
    public startDate: Date,
    public status: string,
    public devices: Device[],
    public endDate?: Date
  ) {
    this.startDate = new Date(Date.now());
  }

}
