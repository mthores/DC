export class Todo {
  public _id: string;
  public item: string;
  public done: boolean;

  constructor(_id: string, item: string, done: string) {
    this._id = _id;
    this.item = item;
    if (done === 'true') {
      this.done = true;
    } else {
      this.done = false;
    }
  }
}
