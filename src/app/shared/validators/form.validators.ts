import { FormControl } from '@angular/forms';


export class FormValidator {
  static getLettersOnlyValidator() {
    return function (control: FormControl) {
      if (control.value !== null) {
        if (!control.value.match(/^[a-z, ,A-Z,æÆøØåÅ]*$/) && control.value !== '') {
          return { 'onlyLettersAllowedError': true };
        }
      }
    };
  }
  static getNumbersOnlyValidator() {
    return function (control: FormControl) {
      if (control.value !== null) {
        if (!control.value.match(/^\d+$/) && control.value !== '') {
          return { 'onlyNumbersAllowedError': true };
        }
      }
    };
  }



}
