import { TicketService } from 'app/services/ticket.service';
import { TicketsComponent } from 'app/tickets/tickets.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Ticket } from 'app/shared/models/ticket.model';
import { FormGroup, ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms';
import { FormValidator } from 'app/shared/validators/form.validators';

@Component({
  selector: 'app-ticket-form',
  templateUrl: './ticket-form.component.html',
  styleUrls: ['./ticket-form.component.scss']
})
export class TicketFormComponent {
  public ticketForm: FormGroup;
  submitFailed = false;
  @Input() ticket: Ticket;
  @Input() edit: boolean;
  @Output() close = new EventEmitter<boolean>();
  @Output() clearFilter = new EventEmitter();

  constructor(fb: FormBuilder, private ticketService: TicketService) {
    this.ticketForm = fb.group({
      'createdBy_id': ['', [Validators.required, FormValidator.getLettersOnlyValidator()]],
      'service_id': ['', FormValidator.getNumbersOnlyValidator()],
      'title': ['', Validators.required],
      'description': ['', Validators.required],
      'status': ['Open', Validators.required],
      'devices': ['', Validators.required],
    });
    this.edit = false;
  }

  public toggleEdit() {
    this.edit = !this.edit;
  }
  closeOverlay() {
    this.close.emit(true);
  }

  submitTicketForm(): void {
    if (this.ticketForm.valid) {
      let ticketOfForm = this.updateTicket();

      if (ticketOfForm._id === null) {
        this.ticketService.addNewTicket(ticketOfForm);
      } else {
        this.ticketService.editExsistingTicket(ticketOfForm);
      }

      this.ticket = ticketOfForm;
      this.toggleEdit();
      this.clearFilter.emit();
      this.closeOverlay();

    } else {
      this.ticketForm.get('title').markAsTouched();
      this.ticketForm.get('devices').markAsTouched();
      this.ticketForm.get('createdBy_id').markAsTouched();
      this.ticketForm.get('description').markAsTouched();
      console.log('form not valid');
    }
  }

  updateTicket(): Ticket {
    let newTicket: Ticket = new Ticket(
      this.ticketForm.value.createdBy_id,
      this.ticket._id,
      this.ticket.ticket_id,
      this.ticketForm.value.service_id,
      this.ticketForm.value.title,
      this.ticketForm.value.description,
      this.ticket.startDate,
      this.ticketForm.value.status,
      this.ticketForm.value.devices,
      this.ticket.endDate);
      newTicket.startDate = this.ticket.startDate;
    return newTicket;
  }

}
