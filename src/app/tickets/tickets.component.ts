import { Component, OnInit } from '@angular/core';
import { TicketService } from 'app/services/ticket.service';
import { Ticket } from 'app/shared/models/ticket.model';
import { TicketsPipe } from 'app/shared/filters/tickets.pipe';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})

export class TicketsComponent {
  tickets: Ticket[] = [];
  selectedTicket: Ticket;
  edit = false;
  filterTable = '';

  constructor(private ticketService: TicketService) {
      this.tickets = ticketService.tickets;
  }

  selectTicket(ticket: Ticket) {
    this.edit = false;
    let ticketCopy = Object.assign({}, ticket);
    this.selectedTicket = ticketCopy;
  }
  createTicket() {
    const newTicket: Ticket = Ticket.createNewTicket();
    this.selectedTicket = newTicket;
    this.edit = true;
  }
  clearFilter(event) {
    this.filterTable = '';
  }
  onClose(closeNow: boolean) {
    if (closeNow) {
      this.selectedTicket = null;
      this.edit = false;
    }
  }
}
