import { Todo } from 'app/shared/models/todo.model';
import { Component, OnInit, } from '@angular/core';
import { TodoService } from 'app/services/todo.service';
import { Response } from '@angular/http';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  todoList: Todo[] = [];
  clearInput: String = '';

  constructor(private todoService: TodoService) {
    this.onRead();
  }
    clear() {
    this.clearInput = null;
  }

  onRead() {
    this.todoService.getTodos().subscribe(response => {
      let t = response.filter(todo => {
        if (todo.item && todo.done !== undefined || null) {
          return todo;
        }
      });
      this.todoList = [];
      for (let i = 0; i < t.length; i++) {
        this.todoList.push(new Todo(t[i]._id, t[i].item, t[i].done));
      }
    });
  }

  onDelete(todo): void {
    this.todoService.deleteTodo(todo._id).subscribe(
      () => {
        this.onRead();
      }
    );
  }

  onUpdate(todo: Todo) {
    this.todoService.putTodo(todo).subscribe(
      () => {
        this.onRead();
      }
    );
  }

  onCreate(item: String, done: boolean) {
    this.todoService.postTodo(item, done = false).subscribe(
      () => {
        this.onRead();
        this.clear();
      }
    );
  }


  ngOnInit() { }

}
